# Evaluation 
durchgeführt am 09.07.2018

## Query: Leistung Subventionen vom Staat
Intention: Man ist Privatperson/Unternehmer und möchte Wissen welche Unterstützung er vom Staat erwarten kann.

|Gesetzbuch/Verordnung|Paragraph|Relevant|precision|recall|
|:--:|:--:|:--:|:--:|:--:|
|SubvG|§ 3|1|1|1|
|WZG§35ISRBek|(XXXX)|0|0.5|1|
|WZG§35ESPBek|(XXXX)|0|0.33|1|
|HdlStatG|§ 6|0|0.25|1|
|WZG§35AUSBek|(XXXX)|0|0.2|1|
|WZG§35IRLBek|(XXXX)|0|0.17|1|
|WZG§35NLDBek|(XXXX)|0|0.14|1|
|DDR-EErfG|§ 7|0|0.13|1|
|WZG§35ISLBek|(XXXX)|0|0.11|1|
|WZG§35NZLBek |(XXXX)|0|0.1|1|

## Query: Was für Einkommen müssen auf den Sozialamt angegeben werden? 
Intention: Ich muss (aus welchen Gründen auch immer) einen Antrag beim Amt für Soziales einreichen, wobei die Angabe des Einkommens verlangt ist. Welche Angaben sind alles zu machen?

|Gesetzbuch/Verordnung|Paragraph|Relevant|precision|recall|
|:--:|:--:|:--:|:--:|:--:|
|StBAPO 1977|§ 38|0|0|0|
|BEEG|§ 8|0|0|0|
|DBPDirBeamtRAnO|(XXXX)|0|0|0|
|49. AnrV|§ 4|0|0|0|
|AktG|§ 80|0|0|0|
|GmbHG|§ 35a|0|0|0|
|TMG|§ 6|0|0|0|
|StPO|§ 267|0|0|0|
|FoVG|§ 12|0|0|0|
|12. ProdSV|§ 8|0|0|0|

## Query: Ärztliche Prüfung Voraussetzung
Intention: Ich möchte Arzt werden. Was benötige ich alles/ muss ich alles wissen?

|Gesetzbuch/Verordnung|Paragraph|Relevant|precision|recall|
|:--:|:--:|:--:|:--:|:--:|
|ÄApprOÄndV 5|§ 1|0|0|0|
|ÄApprO 2002|Anlage 12|0|0|0|
|ZÄPrO|Anlage 5|0|0|0|
|ÄApprO 2002|§ 13|1|0.25|0.5|
|ÄApprO 2002|§ 1|1|0.4|1|
|DruckLV|Inhaltsverzeichnis|0|0.33|1|
|AFuV|§ 7|0|0.29|1|
|ÄApprOÄndV 7 |§ 4|0|0.25|1|
|AltPflG|§ 4a|0|0.22|1|
|AltPflG|§ 10|0|0.2|1|

## Query: Meisterprüfung Modellbauer
Intention: Man ist gelernter Modellbauer und möchte seinen Meister machen. Wie st die Prüfung aufgebaut? Wer darf sie abhalten?...

|Gesetzbuch/Verordnung|Paragraph|Relevant|precision|recall|
|:--:|:--:|:--:|:--:|:--:|
|MbauMstrV|§ 1|1|1|0.25|
|MbauMstrV|§ 3|1|1|0.5|
|MbauMstrV|§ 6|1|1|0.75|
|ModellBTechAusbV|§ 1|0|0.75|0.75|
|MbauMstrV|§ 2|0|0.6|0.75|
|MeistPrFRGlV|Anlage|0|0.5|0.75|
|MPVerfVO|§ 21|0|0.43|0.75|
|LwMstrPrV|§ 2|0|0.38|0.75|
|LwMstrPrV|§ 12|1|0.44|1|
|GalvMstrV|§ 1|0|0.4|1|

## Query: Bezahlung Soldaten Sondereinkommen Zuschläge
Intention: Ich will Soldat werden. Was kann man (einkommensmäßig) erwarten, oder wer legt es fest, was gibt es sonst noch zu beachten?

|Gesetzbuch/Verordnung|Paragraph|Relevant|precision|recall|
|:--:|:--:|:--:|:--:|:--:|
|SGB 6|§ 66|1|1|0.33|
|GewO|§ 6|1|1|0.66|
|BGB|BGB|0|0.66|0.66|
|AuslZuschlV|§ 2|1|0.75|1|
|NutzZG|§ 2|0|0.6|1|
|SGB 6|§ 76|0|0.5|1|
|SGB 6|§ 264b|0|0.48|1|
|StRSaarEG|§ 49|0|0.38|1|
|SGB 6|§ 244a|0|0.33|1|
|BBesG|Inhaltsübersicht|0|0.3|1|

## Query: Unterlagen Dokumente Eichungen
Intention: Gesucht wird Aufbewahrungszeit, Form, Inhalt

|Gesetzbuch/Verordnung|Paragraph|Relevant|precision|recall|
|:--:|:--:|:--:|:--:|:--:|
|MessEV|§ 52|1|1|0.5|
|MessEV|§ 43|1|1|1|
|MessEG|§ 54|0|0.66|1|
|MessEV|§ 53|0|0.5|1|
|SeeBewachDV|§ 10|0|0.4|1|
|MessEGebV|§ 2|0|0.33|1|
|MessEV|§ 42|0|0.29|1|
|EUZBBGVbg|VIII|0|0.25|1|
|BeurkG|§ 56|0|0.22|1|
|GGVSee|§ 19|0|0.2|1|

## Query: Klage gegen Zoll
Intention: Zuständiges Gericht; 

|Gesetzbuch/Verordnung|Paragraph|Relevant|precision|recall|
|:--:|:--:|:--:|:--:|:--:|
|PatG|§ 81|0|0|0|
|JuSchG|§ 25|0|0|0|
|ZPO|§ 25|0|0|0|
|VwGO|§ 51|0|0|0|
|ZKG|§ 51|0|0|0|
|SGG|§ 100|0|0|0|
|ZPO|§ 33|0|0|0|
|BRAO|§ 112f|0|0|0|
|VwGO|§ 195|0|0|0|
|VwGO|§ 89|0|0|0|

## Query: Gerichtskosten tragen
Intention: Wer? Wann? Wo?

|Gesetzbuch/Verordnung|Paragraph|Relevant|precision|recall|
|:--:|:--:|:--:|:--:|:--:|
|FamFG|§ 183|1|1|0.14|
|FGO|§ 136|1|1|0.29|
|SGG|§ 193|1|1|0.43|
|VwGO|§ 155|1|1|0.57|
|GNotKG|§ 130|1|1|0.71|
|PAO|§ 43b|0|0.83|0.71|
|WiPrO|§ 55a|0|0.71|0.71|
|ZPÜbkHaagG|§ 8|1|0.86|0.86|
|VollstrVtrTUNAG|§ 11|0|0.66|0.86|
|ZPO|§ 125|1|0.7|1|

## Query: Daten übermitteln Behörden 
Intention: Welche Daten können zwischen Behörden übermittelt werden und von wem?

|Gesetzbuch/Verordnung|Paragraph|Relevant|precision|recall|
|:--:|:--:|:--:|:--:|:--:|
|BPolG|§ 32|1|1|0.1|
|SeeFischG|§ 7|1|1|0.2|
|CWÜAG|§ 6|1|1|0.3|
|ESVG|§ 9|1|1|0.4|
|BVerfSchG|§ 21|1|1|0.5|
|HolzSiG|§ 4|1|1|0.6|
|ZFdG|§ 33|1|1|0.7|
|SchwarzArbG|§ 6|1|1|0.8|
|AZRG|§ 30|1|1|0.9|
|DIMDIAMV|§ 2|1|1|1|

## Query: Vertreten Entschädigung Gericht
Intention: Wer darf mich alles bei einem Rechtsstreit über eine Entschädigung vertreten?

|Gesetzbuch/Verordnung|Paragraph|Relevant|precision|recall|
|:--:|:--:|:--:|:--:|:--:|
|StPo|§ 430|0|0|0|
|ArbGG|§ 12a|0|0|0|
|WiPrüfVO|§ 2|0|0|0|
|WertAusglG|§ 12|0|0|0|
|VSchDG|§ 17|1|0.2|1|
|ZPO|§ 510b|0|0.17|1|
|StrEG|§ 8|0|0.14|1|
|BGB|§ 351f|0|0.13|1|
|StrEG|§ 14|0|0.11|1|
|KSVG|$ 38|0|0.1|1|

## Query: Aufgaben der Bundesämtern
Intention: Welche Aufgaben haben die Bundesämter?

|Gesetzbuch/Verordnung|Paragraph|Relevant|precision|recall|
|:--:|:--:|:--:|:--:|:--:|
|SBGWV|§ 20|0|0|0|
|DRiG|§ 4|0|0|0|
|See-BAV|§ 21|0|0|0|
|AtSMV|§ 3|0|0|0|
|BRAO|§ 163|0|0|0|
|PflVG|§ 13a|0|0|0|
|EnWG|§ 64a|0|0|0|
|SGB 11|§ 52|0|0|0|
|SGB 8|§ 76|0|0|0|
|SchwarzArbG|§ 15|0|0|0|

## Query: Definitionen in Verordnungen
Intention: Welche Begriffe sind alles rechtlich genau definiert?

|Gesetzbuch/Verordnung|Paragraph|Relevant|precision|recall|
|:--:|:--:|:--:|:--:|:--:|
|EEMD-ZVAnl I|§ 1|0|0|0|
|NachwV|§ 18|0|0|0|
|EEMD-ZVAnl II|§ 1|0|0|0|
|TKG|§ 84|0|0|0|
|UStBMG|Art 9|0|0|0|
|BKGG|§ 17|0|0|0|
|SVSaarAnglG|§ 31|0|0|0|
|MaStRV|§ 22|0|0|0|
|EinhZeitG|§ 3|0|0|0|
|AuslWBG|§ 77|0|0|0|

## Query: Steueranmeldung
Intention:

|Gesetzbuch/Verordnung|Paragraph|Relevant|precision|recall|
|:--:|:--:|:--:|:--:|:--:|
|AO 1977|§ 168|1|1|0.1|
|EnergieStV|§ 37a|1|1|0.2|
|VersStG|§ 8|1|1|0.3|
|KaffeeStG|§ 12|1|1|0.4|
|AO 1977|§ 355|1|1|0.5|
|SchaumwZwStG|§ 15|1|1|0.6|
|LuftVStG|§ 12|1|1|0.7|
|EnergieStG|§ 33|1|1|0.8|
|StromStV|§ 5|1|1|0.9|
|RennwLottGABest|§ 31a|1|1|1|

## Query: Ablauf einer Wahl
Intention: Wie läuft eine (Bundes-)Wahl ab?

|Gesetzbuch/Verordnung|Paragraph|Relevant|precision|recall|
|:--:|:--:|:--:|:--:|:--:|
|WODrittelbG|§ 11|0|0|0|
|BEG|§ 84|0|0|0|
|BGB|§ 264|0|0|0|
|WOSprAuG|§ 8|0|0|0|
|SachenRBerG|§ 16|0|0|0|
|BEG|§ 96|0|0|0|
|DWG|§ 29|0|0|0|
|SchwbVWO|§ 7|0|0|0|
|GleibWV|§ 4|0|0|0|
|SprAuG|§ 7|0|0|0|

## Query: Worüber hat das Innenministerium Aufsicht?
Intention: 

|Gesetzbuch/Verordnung|Paragraph|Relevant|precision|recall|
|:--:|:--:|:--:|:--:|:--:|
|GrÄndStVtr2 ND/NW|Schlussformel|0|0|0|
|ZPO|§ 64|0|0|0|
|KatHiLAbkPOLG|Art 2|0|0|0|
|KatHiLAbkCZEG|Art 2|0|0|0|
|GrÄndStVtr MV/ND|Art 6|0|0|0|
|AWaffV|§ 10|0|0|0|
|HwO|§ 75|0|0|0|
|FZV|Anlage 3|0|0|0|
|EGBusDV|§ 6|0|0|0|
|SGB 4|§ 90|1|0.1|1|

## Query: Ausbildung Rechte Pflichten
Intention: Welche Rechte/ Pflichteh habe ich als Auszubildener?

|Gesetzbuch/Verordnung|Paragraph|Relevant|precision|recall|
|:--:|:--:|:--:|:--:|:--:|
|PAO|§ 12|0|0|0|
|SGB 1|§ 12|1|0.5|1|
|GAD|Inhaltsübersicht|0|0.33|1|
|PatAnwAPrV |Inhaltsübersicht|0|0.25|1|
|SGB 7|§ 120|0|0.2|1|
|SVOrgSaarG|§ 4|0|0.17|1|
|WoEigG|§ 43|0|0.14|1|
|AdenauerHStiftG|§ 13|0|0.13|1|
|BWaldG|§ 24|0|0.11|1|
|ZDG|§ 25c|0|0.1|1|

## Query: Zugriff öffentlich erhobener Daten
Intention: Welche Daten und im welchem Umfang sind diese zugänglich?

|Gesetzbuch/Verordnung|Paragraph|Relevant|precision|recall|
|:--:|:--:|:--:|:--:|:--:|
|BDSG|§ 47|0|0|0|
|KonzVgV|§ 8|0|0|0|
|SektVO|§ 10|0|0|0|
|ZDPersAV|§ 10|0|0|0|
|VgV 2016|§ 10|0|0|0|
|RED-G|§ 5|0|0|0|
|VWDG|§ 11|0|0|0|
|ATDG|§ 5|0|0|0|
|ZensVorbG 2021|§ 10|0|0|0|
|ARegV|§ 29|0|0|0|

## Query: Grenzwerte am Arbeitsplatz
Intention: Welche Grenzwerte gelten für welche Stoffe an welchen Arbeitsstoffen. Wie werden diese festgelegt.

|Gesetzbuch/Verordnung|Paragraph|Relevant|precision|recall|
|:--:|:--:|:--:|:--:|:--:|
|OStrV|§ 8|0|0|0|
|TrinkwV|§ 8|0|0|0|
|GasNZV|§ 24|0|0|0|
|FSPersAV|§ 8|0|0|0|
|AZV|§ 10|0|0|0|
|EMFV|§ 2|1|0.17|1|
|MuSchSoldV|§ 3a|0|0.14|1|
|BEMFV|§ 3|0|0.13|1|
|BEMFV|§ 8|0|0.11|1|
|GArchDVDV|§ 16|0|0.1|1|

## Query: Handelsregister Führung
Intention: Wie wird das Handelsrigister geführt/gespeichert?

|Gesetzbuch/Verordnung|Paragraph|Relevant|precision|recall|
|:--:|:--:|:--:|:--:|:--:|
|eBAnzV|§ 1|0|0|0|
|GenRegV|§ 1|0|0|0|
|HRV|§ 51|1|0.33|0.5|
|TBelV|§ 1|0|0.25|0.5|
|HRV|§ 54|1|0.4|1|
|HGB|§ 8a|0|0.33|1|
|UAGBV|§ 1|0|0.29|1|
|MautSysG|§ 6|0|0.25|1|
|SpTrUG|§ 8|0|0.22|1|
|PfandBG|§ 33|0|0.2|1|

## Query: Anträge auf Sozialleistungen
Intention: Wann, wie, wo wer.

|Gesetzbuch/Verordnung|Paragraph|Relevant|precision|recall|
|:--:|:--:|:--:|:--:|:--:|
|SGB 1|§ 36|1|1|0.33|
|SGB 1|§ 16|1|1|0.66|
|SGB 2|§ 12a|1|1|1|
|RAG 17|§ 17|0|0.75|1|
|SGB 1|§ 39|0|0.6|1|
|RAG 19|§ 27|0|0.5|1|
|RAG 18|§ 20|0|0.43|1|
|SGB X|§ 117|0|0.38|1|
|SGB 12|§ 2|0|0.33|1|
|SGB X |§ 115|0|0.3|1|
