---
title: "Word frequency"
output: html_notebook
---

Packages (only run once):

```{r}
install.packages('tm')
install.packages('NLP')
```

Libraries (run each session):

```{r}
library(tm)
```

Import text:

```{r}
#might need to be .txt instead of xml. 
text <- readLines(file.choose())
docs <- Corpus(VectorSource(text))
toSpace <- content_transformer(function (x , pattern ) gsub(pattern, " ", x))
docs <- tm_map(docs, toSpace, "/")
docs <- tm_map(docs, toSpace, "@")
docs <- tm_map(docs, toSpace, "\\|")

#optional german_stopwords_plain (contains less stopwords) or leave empty for no stopword cleaning OR if you want you can put any stopwordlist you find in there
stopwords.list <- 'german_stopwords_full.txt'
stopwords <- read.csv(stopwords.list, header=F, sep="", stringsAsFactors=F)[,1]
```

Clean corpus (optional):

```{r}
docs <- tm_map(docs, content_transformer(tolower))
docs <- tm_map(docs, removeNumbers)
docs <- tm_map(docs, removeWords, stopwords) 
docs <- tm_map(docs, removePunctuation)
docs <- tm_map(docs, stripWhitespace)
```

Count words:

```{r}
dtm <- TermDocumentMatrix(docs)
m <- as.matrix(dtm)
v <- sort(rowSums(m),decreasing=TRUE)
df <- data.frame(word = names(v),freq=v)
```

Show wordfrequency:

```{r}
df
```

If you want to only show the top x words:

```{r}
# replace x with whatever number you want
head(df, x)
```