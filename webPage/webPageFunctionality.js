
var jsonRes;
var query;

//pressing ENTER to trigger search
document.getElementById("sbar").addEventListener("keyup", function(event) {
  event.preventDefault();
  if (event.keyCode == 13) {
    query = document.getElementById("sbar").value.split(" ");
    searchButton();
  }
});

//sending request to server and calling createResults, to present the results
function searchButton() {
  var input = document.getElementById("sbar").value;
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      jsonRes = JSON.parse(this.responseText);
      createResults(jsonRes);
      //document.getElementById("results").innerHTML = this.responseText;
      //window.alert(xhttp.getResponseHeader("Accept-Charset"));
    }
  };
  xhttp.open("GET", ("http://temir11.informatik.uni-leipzig.de:8081/lawsearch/search?query=" + input), true);
  //xhttp.setRequestHeader("Accept-Charset", "utf-8");
  //xhttp.setRequestHeader("Content-Type","text/html; charset=utf-8");
  xhttp.send();
}

//showing the results in the "results" paragraph
function createResults(myJson) {
  var place = document.getElementById("results");

  while(place.hasChildNodes()) {
    place.removeChild(place.firstChild);
  }

  if(myJson.result.length == 0) {
    var elem = document.createElement("P");
    var result = document.createTextNode("...keine Ergebnisse gefunden...");

    elem.appendChild(result);
    elem.setAttribute("class", "centered");

    place.appendChild(elem);
  }
  else {
    for (i in myJson.result) {
      var name = myJson.result[i].shortname + " : " + myJson.result[i].paragraph;
      var elem1 = document.createElement("H2");
      var title = document.createTextNode(name);

      elem1.setAttribute("id", "" + i);
      elem1.setAttribute("onclick", "toggleResult("+i+")");
      elem1.appendChild(title);

      place.appendChild(elem1);

      var elem2 = document.createElement("P");
      
      elem2.setAttribute("class", "result");
      elem2.innerHTML = myJson.result[i].text;

      place.appendChild(elem2);
    }
  }
}

//shows the results as window (?)
function toggleResult(id) {
  var elem = document.getElementById(id).nextElementSibling;
  if(elem.getAttribute("class") == "result") {
    elem.setAttribute("class", "showResult");
  }
  else elem.setAttribute("class", "result");
}
