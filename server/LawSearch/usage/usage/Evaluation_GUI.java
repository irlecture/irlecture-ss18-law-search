/**
 *
 */
package usage;

import java.awt.Dimension;
import java.awt.Toolkit;

import evaluation.Evaluation;
import javafx.application.Application;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * @author Sven Landmann
 *
 */
public class Evaluation_GUI extends Application {

	private int queryCount;
	private int progress = 0;

	private Evaluation eval;

	private Label headline;

	private WebView browser;
	private WebEngine engine;

	private ProgressBar bar;

	private Button yes;
	private Button no;
	private Button start;

	private Scene scene;
	private SplitPane splitpane;
	private StackPane headPane;
	private StackPane middle;
	private StackPane bottom;

	@Override
	public void init() {
		Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
		bar = new ProgressBar(0);
		bar.setVisible(false);
		bar.setPrefWidth(screensize.getWidth() * 0.5);
		eval = new Evaluation(ConstantForUsage.CORPUS, ConstantForUsage.PATHTOEVALUATIONXML,
				ConstantForUsage.PATHTOINDEX, ConstantForUsage.PATHTOSTOPWORDLIST);
		eval.init();
		queryCount = eval.getQueryCount();

		headline = new Label("Evaluation");

		StackPane.setAlignment(headline, Pos.CENTER);

		yes = new Button("Relevant");
		yes.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				eval.UserSayIsRelevant();
				engine.loadContent("<html><head></head><body></body></html>");
				run();
			}
		});
		yes.setVisible(false);
		yes.setPrefHeight(screensize.getHeight() * 0.05);
		yes.setPrefWidth(screensize.getHeight() * 0.1);

		StackPane.setAlignment(yes, Pos.BOTTOM_LEFT);

		no = new Button("nicht Relevant");
		no.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				eval.UserSayIsNotRelevant();
				engine.loadContent("<html><head></head><body></body></html>");
				run();

			}
		});
		no.setVisible(false);
		StackPane.setAlignment(no, Pos.BOTTOM_RIGHT);
		no.setPrefHeight(screensize.getHeight() * 0.05);
		no.setPrefWidth(screensize.getHeight() * 0.1);

		start = new Button("Start");
		start.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				start.setVisible(false);
				no.setVisible(true);
				bar.setVisible(true);
				yes.setVisible(true);
				run();
			}
		});
		start.setPrefHeight(screensize.getHeight() * 0.05);
		start.setPrefWidth(screensize.getHeight() * 0.1);

		splitpane = new SplitPane();
		headPane = new StackPane();

		headPane.getChildren().add(headline);

		middle = new StackPane();

		bottom = new StackPane();
		bottom.getChildren().addAll(yes, no, start, bar);

		splitpane.getItems().addAll(headPane, middle, bottom);
		splitpane.setOrientation(Orientation.VERTICAL);
		splitpane.setDividerPositions(0.1, 0.9, 1);

	}

	@Override
	public void start(Stage stage) {

		stage.setFullScreen(true);
		stage.centerOnScreen();
		StackPane root = new StackPane();
		browser = new WebView();
		engine = browser.getEngine();
		middle.getChildren().add(browser);
		root.getChildren().add(splitpane);
		scene = new Scene(root, 500, 500);

		stage.setTitle("Evaluation");
		stage.setScene(scene);
		stage.show();
	}

	@Override
	public void stop() {
	}

	public static void main(String[] parameters) {
		launch(parameters);
	}

	private void run() {
		while (eval.nextInquery()) {
			while (eval.nextResult()) {
				if (!eval.relevanceDecidable()) {
					userInput();
					return;
				}
			}
			progress++;
			bar.setProgress((double) progress / (double) queryCount);
		}

		eval.saveResults();
		eval.close();
		System.exit(0);
	}

	private void userInput() {
		engine.loadContent("<html><head></head><body>" + "<br>Query: " + eval.getInquiry().getQuery() + "</br>"
				+ "<br>Zweck: " + eval.getInquiry().getPurpose() + "</br>" + "<br>Gesetzbuch: "
				+ eval.getLaw().getLawbook() + "</br>" + "<br>" + eval.getLaw().getHeadline() + "</br>"
				+ "<br>Paragraf: " + eval.getLaw().getParagraph() + "</br>" + "<br>Gesetzestext: </br>"
				+ eval.getLaw().getText() + "</body></html>");

	}

}
