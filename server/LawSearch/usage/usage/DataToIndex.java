package usage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.lucene.store.FSDirectory;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import indexing.Indexer;
import indexing.IndexerBRD;
import indexing.XML_Laws_BRD_ContentHandler_oldVErsion;

public class DataToIndex {

	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
		File inputDirectory = ConstantForUsage.PATHTODATA.toFile();
		Scanner scan = new Scanner(System.in);

		BufferedReader stopWords = new BufferedReader(new FileReader(ConstantForUsage.PATHTOSTOPWORDLIST.toString()));
		Indexer indexer = new IndexerBRD(FSDirectory.open(ConstantForUsage.PATHTOINDEX), stopWords);
		stopWords.close();

		XMLReader reader = XMLReaderFactory.createXMLReader();
		reader.setEntityResolver(new XML_DTD_Load_Inhibitor_Resolver());
		XML_Laws_BRD_ContentHandler_oldVErsion contentHandler = new XML_Laws_BRD_ContentHandler_oldVErsion(indexer);

		reader.setContentHandler(contentHandler);

		for (File file : inputDirectory.listFiles()) {
			reader.parse(file.getAbsolutePath());
		}

		indexer.close();
		System.out.println("all in index");

		System.out.println("Bitte geben Sie dem Corpus einen Namen:\n");
		String corpusName = scan.nextLine();
		System.out.println("In welche Sprache ist der Corpus:\n");
		String language = scan.nextLine();
		System.out.println("Geben sie einen Schl�ssel f�r den Corpus ein:\n");
		String key = scan.nextLine();
		indexer.createProperty(corpusName, language, key, ConstantForUsage.PATHTOSTOPWORDLIST.getFileName().toString(),
				ConstantForUsage.PATHTOINDEX);
		scan.close();

	}
}
