/**
 *
 */
package usage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.store.FSDirectory;

import com.google.protobuf.util.JsonFormat;

import generated.LawSearchAPI.Law;
import generated.LawSearchAPI.Request;
import generated.LawSearchAPI.Response;
import lawsearch.LawSearch;
import searcher.Searcher;

/**
 * @author Sven Landmann
 *
 */
public class Console_Search {

	/**
	 * @param args
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public static void main(String[] args)
			throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		Scanner scan = new Scanner(System.in);
		Class<?> searcherClass = Class.forName("searcher.Searcher" + ConstantForUsage.CORPUS.toUpperCase());
		Searcher searcher = (Searcher) searcherClass.newInstance();
		BufferedReader stopWords = new BufferedReader(new FileReader(ConstantForUsage.PATHTOSTOPWORDLIST.toFile()));

		searcher.init(DirectoryReader.open((FSDirectory.open(ConstantForUsage.PATHTOINDEX))), stopWords);
		stopWords.close();

		LawSearch ls = new LawSearch("brd", searcher);
		String input;

		System.out.println("Bitte geben Sie eine Query ein: (q zum beenden)\n");
		Request.Builder request = Request.newBuilder();
		request.setCorpus("brd");

		request.setPage(1);
		request.setResults(10);

		while (true) {
			input = scan.nextLine();
			if ("q".equals(input))
				break;
			request.setQuery(input);
			Response rp = ls.search(request.build());

			for (Law law : rp.getResultList()) {
				JsonFormat.printer().includingDefaultValueFields().appendTo(law, System.out);
				System.out.println("\n\n");
			}
			System.out.println("Bitte geben Sie eine Query ein: (q zum beenden)\n");
		}
		scan.close();
		System.out.println("\n\nAuf Wiedersehen");
	}

}
