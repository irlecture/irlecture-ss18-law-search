package usage;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Random;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.FSDirectory;

import lawsearch.DocumentConstants;

public class RandomDocumentGetter {

	public static void main(String[] args) throws IOException {
		Random zufall = new Random();
		zufall.setSeed(System.currentTimeMillis());
		FSDirectory dir = FSDirectory.open(ConstantForUsage.PATHTOINDEX);
		DirectoryReader reader = DirectoryReader.open(dir);
		IndexSearcher searcher = new IndexSearcher(reader);
		int anzahl = reader.numDocs();

		if (args.length > 2) {
			anzahl = Integer.parseInt(args[1]);
			for (int i = 2; i < args.length; i++) {
				save(zufall, "Charge" + i, ConstantForUsage.PATHTOEVALUATIONLAWS, anzahl, searcher);
			}
		}
	}

	public static void save(Random zufall, String name, Path path, int anzahl, IndexSearcher searcher)
			throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(path.resolve(name + ".txt").toFile()));

		for (int i = 0; i < 20; i++) {
			int docNum = zufall.nextInt(anzahl);
			Document doc = searcher.doc(docNum);
			bw.write(doc.getField(DocumentConstants.CONTENT).stringValue());
			bw.write(System.lineSeparator());
			bw.write(System.lineSeparator());

		}

		bw.close();
	}
}
