package usage;

import java.io.IOException;
import java.io.StringReader;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;;

public class XML_DTD_Load_Inhibitor_Resolver implements EntityResolver {

	@Override
	public InputSource resolveEntity(String arg0, String arg1) throws SAXException, IOException {
		return new InputSource(new StringReader(""));
	}

}
