var logMessage = null;
var time;
var jsonRes = null;
var clickedDocs = [];

//pressing ENTER to trigger search
document.getElementById("sbar").addEventListener("keyup", function(event) {
  event.preventDefault();
  if (event.keyCode == 13) {
    searchButton();
  }
});

window.onunload = function(){
  logClient();
}

document.getElementById("sbar").addEventListener("change", function(event){
  event.preventDefault();
  jsonRes == null;
})

function logClient(){
  if(logMessage != null){
  var xhttp = new XMLHttpRequest();
  var logging = "log=" + logMessage + "&time=" + Math.round(performance.now() - time) + "&docs=" + clickedDocs[0] ;
  for(i = 1; i< clickedDocs.length; i++){
    logging += " ";
    logging += clickedDocs[i];
  }
  xhttp.onreadystatechange = function() { 
  };
  xhttp.open("POST", "http://localhost:8080/lawsearch/search?" + logging , true)
  xhttp.send();
  logMessage = null;
  clickedDocs = [];
  time = null;
  }
}

function createRequest(){
  var requestJson =  requestJson = "query=" + document.getElementById("sbar").value +
        "&result=" + 10 + "&page=" + 1 ;
  return requestJson;
}


function createRequestNext(){
  var requestJson = "query=" + document.getElementById("sbar").value + "&results=" + 10
  + "&page=" + (parseInt(jsonRes.page) + 1) + "&doc=" + jsonRes.doc +
   "&score=" + jsonRes.score;

   return requestJson
}

//sending request to server and calling createResults, to present the results
function searchButton() {
  logClient(); 

  var input = document.getElementById("sbar").value;
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      time = performance.now();
      jsonRes = JSON.parse(this.responseText);
      createResults(jsonRes,1,10);
      document.getElementById("buttonNext").setAttribute("style","");
      //document.getElementById("results").innerHTML = this.responseText;
      //window.alert(xhttp.getResponseHeader("Accept-Charset"));
    }
  };
  xhttp.open("GET", ("http://localhost:8080/lawsearch/search?" + createRequest()), true);
  //xhttp.setRequestHeader("Accept-Charset", "utf-8");
  //xhttp.setRequestHeader("Content-Type","text/html; charset=utf-8");
  xhttp.send();
}

//sending request to server and calling createResults, to present the results
function searchNext() {
  var input = document.getElementById("sbar").value;
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      jsonRes = JSON.parse(this.responseText);
      createResults(jsonRes, jsonRes.page, 10);
      //document.getElementById("results").innerHTML = this.responseText;
      //window.alert(xhttp.getResponseHeader("Accept-Charset"));
    }
  };
  xhttp.open("GET", ("http://localhost:8080/lawsearch/search?" + createRequestNext()), true);
  //xhttp.setRequestHeader("Accept-Charset", "utf-8");
  //xhttp.setRequestHeader("Content-Type","text/html; charset=utf-8");
  xhttp.send();
}

//showing the results in the "results" paragraph
function createResults(myJson, page, results) {
  var place = document.getElementById("results");
  if(page == 1){
    while(place.hasChildNodes()) {
      place.removeChild(place.firstChild);
    }
  }

  if(myJson.result.length == 0) {
    var elem = document.createElement("P");
    if(page > 1){
      var result = document.createTextNode("...keine weiteren Ergebnisse gefunden...");
      document.getElementById("buttonNext").setAttribute("style","display: none")
    }else{
      var result = document.createTextNode("...keine Ergebnisse gefunden...");
      document.getElementById("buttonNext").setAttribute("style","display: none")
    }
    elem.appendChild(result);
    elem.setAttribute("class", "centered");

    place.appendChild(elem);
  }
  else {
    for (i in myJson.result) {
      var name = myJson.result[i].shortname + " : " + myJson.result[i].paragraph;
      var elem1 = document.createElement("H2");
      var title = document.createTextNode(name);
      var headline = document.createTextNode(myJson.result[i].headline);
      if(page > 1){
        elem1.setAttribute("id", "" + (i + (page * results)))
      }else{
        elem1.setAttribute("id", "" + i);
      }
      elem1.setAttribute("docId", myJson.result[i].docnb);
      elem1.setAttribute("onclick", "toggleResult("+i+")");
      elem1.appendChild(title);
      elem1.appendChild(headline);
      
      var elem3 = document.createElement("H3");
      elem3.setAttribute("onclick", "toggleResult("+i+")");
      elem3.appendChild(headline);

      place.appendChild(elem1);
      place.appendChild(elem3);

      var elem2 = document.createElement("P");
      
      elem2.setAttribute("class", "result");
      elem2.innerHTML = myJson.result[i].text;

      place.appendChild(elem2);
    }
  }
}

//shows the results as window (?)
function toggleResult(id) {

  logMessage = document.getElementById("sbar").value;

  var elem = document.getElementById(id).nextElementSibling.nextElementSibling;
  var id = document.getElementById(id).getAttribute("docId");
  if(!clickedDocs.includes(id)){
    clickedDocs.push(id);
  }
  if(elem.getAttribute("class") == "result") {
    elem.setAttribute("class", "showResult");
  }
  else elem.setAttribute("class", "result");
}

