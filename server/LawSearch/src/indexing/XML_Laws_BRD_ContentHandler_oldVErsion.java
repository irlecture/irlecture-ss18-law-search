package indexing;

import java.io.IOException;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import generated.LawSearchAPI.Law;

/**
 * Handler to convert the given xml files to laws.
 *
 * @author Sven Landmann
 *
 */
public class XML_Laws_BRD_ContentHandler_oldVErsion extends DefaultHandler {
	private Indexer indexer;
	private Law.Builder law;
	private boolean norm = false;
	private boolean metadaten = false;
	private boolean jurabk = false;
	private boolean fundstelle = false;
	private boolean periodikum = false;
	private boolean langue = false;
	private boolean text = false;
	private boolean enbez = false;
	private StringBuilder lawText;

	public XML_Laws_BRD_ContentHandler_oldVErsion(Indexer indexer) {
		this.indexer = indexer;
		law = Law.newBuilder();
		lawText = new StringBuilder();
	}

	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) {
		switch (qName) {
		case "norm":
			norm = true;
			law.setDocnb(atts.getValue("doknr"));
			break;
		case "metadaten":
			metadaten = true;
			break;
		case "jurabk":
			jurabk = true;
			break;
		case "fundstelle":
			fundstelle = true;
			break;
		case "periodikum":
			periodikum = true;
			break;
		case "langue":
			langue = true;
			break;
		case "text":
			text = true;
			break;
		case "enbez":
			enbez = true;
			break;

		// TODO Converting XML -> HTML
		case "P":
			lawText.append("<p>");
			break;
		case "DD":
			lawText.append("<dd>");
			break;
		case "DL":
			lawText.append("<dl>");
			break;
		case "DT":
			lawText.append("<dt>");
			break;
		case "table":
			// lawText.append("<table width=\"100%\" style=\"border: none;\">");
			break;
		case "tgroup":
			break;
		case "AttArea":
			break;
		case "FnArea":
			break;
		case "TOC":
			break;
		case "Revision":
			break;
		case "Title":
			break;
		case "Subtitle":
			break;
		case "Kommentar":
			break;
		default:
			break;
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		if (norm) {
			if (metadaten) {
				if (jurabk) {
					law.setShortname(new String(ch, start, length));
				} else if (langue) {
					law.setHeadline(new String(ch, start, length));
				} else if (enbez) {
					law.setParagraph(new String(ch, start, length));
				} else if (fundstelle) {
					if (periodikum) {
						law.setLawbook(new String(ch, start, length));
					}
				}
			} else if (text) {
				lawText.append(ch, start, length);
				// TODO delete if format is set
				lawText.append(" ");
			}
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		switch (qName) {
		case "norm":
			norm = false;
			String newText = lawText.toString();

			if (!law.getParagraph().isEmpty() && !law.getShortname().isEmpty() && !newText.isEmpty()) {
				law.setText(newText);
				try {
					indexer.indexLaw(law.build());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				law.clearParagraph();
				law.clearText();
			}
			lawText = new StringBuilder();
			break;
		case "metadaten":
			metadaten = false;
			break;
		case "jurabk":
			jurabk = false;
			break;
		case "fundstellen":
			fundstelle = false;
			break;
		case "periodikum":
			periodikum = false;
			break;
		case "langue":
			langue = false;
			break;
		case "text":
			text = false;
			break;
		case "enbez":
			enbez = false;
			break;
		// TODO Converting XML -> HTML
		case "P":
			lawText.append("</p>");
			break;
		case "DD":
			lawText.append("</dd>");
			break;
		case "DL":
			lawText.append("</dl>");
			break;
		case "DT":
			lawText.append("</dt>");
			break;
		case "BR":
			lawText.append("<br>");
			break;
		case "table":
			break;
		case "AttArea":
			break;
		case "FnArea":
			break;
		case "TOC":
			break;
		case "Revision":
			break;
		case "Title":
			break;
		case "Subtitle":
			break;
		case "Kommentar":
			break;
		default:
			break;
		}
	}

}
