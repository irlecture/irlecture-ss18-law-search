package evaluation;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

/**
 * Parse an xml File containing all queries and the relevant and not relevant
 * document id's to the Inquiry class object.
 * 
 * @author Sven Landmann
 *
 */
public class EvaluationContentHandler implements ContentHandler {

	private List<Inquiry> inquiries = new ArrayList<>();
	private String currentValue;
	private boolean relevantLaws = false;
	private String corpus;
	private Inquiry inquiry;

	public List<Inquiry> getInquiries() {
		return inquiries;
	}

	public String getCorpus() {
		return this.corpus;
	}

	@Override
	public void characters(char[] ch, int start, int end) throws SAXException {
		currentValue = new String(ch, start, end);
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {

		if (XMLTagsEvaluation.SEARCH.getXmlTag().equals(localName)) {
			inquiry = new Inquiry();
		} else if (XMLTagsEvaluation.RELEVANTDOCUMENTS.getXmlTag().equals(localName)) {
			this.relevantLaws = true;
		} else if (XMLTagsEvaluation.NOTRELEVANTDOCUMENTS.getXmlTag().equals(localName)) {
			this.relevantLaws = false;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (XMLTagsEvaluation.QUERY.getXmlTag().equals(localName)) {
			this.inquiry.setQuery(currentValue);
		} else if (XMLTagsEvaluation.PURPOSE.getXmlTag().equals(localName)) {
			this.inquiry.setPurpose(currentValue);
		} else if (XMLTagsEvaluation.DOCNR.getXmlTag().equals(localName)) {
			if (relevantLaws) {
				this.inquiry.addRelevant(currentValue);
			} else {
				this.inquiry.addNotRelevant(currentValue);
			}
		} else if (XMLTagsEvaluation.SEARCH.getXmlTag().equals(localName)) {
			this.inquiries.add(inquiry);
		}
	}

	@Override
	public void endDocument() throws SAXException {

	}

	@Override
	public void endPrefixMapping(String arg0) throws SAXException {

	}

	@Override
	public void ignorableWhitespace(char[] arg0, int arg1, int arg2) throws SAXException {

	}

	@Override
	public void processingInstruction(String arg0, String arg1) throws SAXException {

	}

	@Override
	public void setDocumentLocator(Locator arg0) {

	}

	@Override
	public void skippedEntity(String arg0) throws SAXException {

	}

	@Override
	public void startDocument() throws SAXException {

	}

	@Override
	public void startPrefixMapping(String arg0, String arg1) throws SAXException {

	}

}
