package evaluation;

import java.io.IOException;
import java.io.Writer;
import java.util.HashSet;
import java.util.Set;

/**
 * Container for an evaluation Query, with the id's of relevant and not relevant
 * documents.
 * 
 * @author Sven Landmann
 *
 */
public class Inquiry {
	private String query;
	private String purpose;
	private Set<String> relevant;
	private Set<String> notRelevant;

	public Inquiry() {
		relevant = new HashSet<>();
		notRelevant = new HashSet<>();
	}

	/**
	 *
	 * @param docNb
	 * @return true if docNb is classified as relevant
	 */
	public boolean isRelevant(String docNb) {
		return relevant.contains(docNb);
	}

	/**
	 *
	 * @param docNb
	 * @return true if docNb is classified as not relevant
	 */
	public boolean isNotRelevant(String docNb) {
		return notRelevant.contains(docNb);
	}

	/**
	 * Adds an document id (docNn) to the relevant documents
	 * 
	 * @param docNb
	 */
	public void addRelevant(String docNb) {
		relevant.add(docNb);
	}

	/**
	 * Adds an document id (docNb) to the not relevant documents
	 * 
	 * @param docNb
	 */
	public void addNotRelevant(String docNb) {
		notRelevant.add(docNb);
	}

	/**
	 * Transform this class to an xml format
	 * 
	 * @param writer
	 * @throws IOException
	 */
	public void ToXML(Writer writer) throws IOException {

		writer.write(XMLTagsEvaluation.SEARCH.getOpenXmlTag());

		writer.write(XMLTagsEvaluation.QUERY.getOpenXmlTag());
		writer.write(this.query);
		writer.write(XMLTagsEvaluation.QUERY.getCloseXmlTag());

		writer.write(XMLTagsEvaluation.PURPOSE.getOpenXmlTag());
		writer.write(this.purpose);
		writer.write(XMLTagsEvaluation.PURPOSE.getCloseXmlTag());

		writer.write(XMLTagsEvaluation.RESULT.getOpenXmlTag());

		writer.write(XMLTagsEvaluation.RELEVANTDOCUMENTS.getOpenXmlTag());

		for (String doc : relevant) {
			writer.write(XMLTagsEvaluation.DOCNR.getOpenXmlTag());
			writer.write(doc);
			writer.write(XMLTagsEvaluation.DOCNR.getCloseXmlTag());
		}

		writer.write(XMLTagsEvaluation.RELEVANTDOCUMENTS.getCloseXmlTag());

		writer.write(XMLTagsEvaluation.NOTRELEVANTDOCUMENTS.getOpenXmlTag());

		for (String doc : notRelevant) {
			writer.write(XMLTagsEvaluation.DOCNR.getOpenXmlTag());
			writer.write(doc);
			writer.write(XMLTagsEvaluation.DOCNR.getCloseXmlTag());
		}

		writer.write(XMLTagsEvaluation.NOTRELEVANTDOCUMENTS.getCloseXmlTag());

		writer.write(XMLTagsEvaluation.RESULT.getCloseXmlTag());

		writer.write(XMLTagsEvaluation.SEARCH.getCloseXmlTag());

	}

	/**
	 *
	 * @return true if the query is not empty
	 */
	public boolean isInitialized() {
		return !query.isEmpty();
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

}
