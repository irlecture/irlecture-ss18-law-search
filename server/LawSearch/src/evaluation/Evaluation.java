package evaluation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.store.FSDirectory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import generated.LawSearchAPI.Law;
import generated.LawSearchAPI.Request;
import searcher.Searcher;
import usage.Evaluation_GUI;

/**
 * Observer for the {@link Evaluation_GUI} class. Use the top k evaluation
 * process to compute the average average precision recall value.
 *
 * @author Sven Landmann
 *
 */
public class Evaluation {
	String corpusName;
	private List<Inquiry> inquiries = new ArrayList<>();
	private List<Double> aap;
	Searcher search;
	private int position = -1;
	private int countRelevantDoc = 0;
	private static final double[] recallPoints = { 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0 };

	private Request.Builder request;

	Law law;
	List<Law> laws;
	Iterator<Law> lawIt;

	Iterator<Inquiry> it;
	Inquiry inquiry;

	double[] precission;
	double[] recall;

	Path input;
	Path index;
	Path stopwords;

	public Evaluation(String corpusName, Path input, Path index, Path stopwords) {
		this.corpusName = corpusName;
		this.input = input;
		this.index = index;
		this.stopwords = stopwords;
		this.aap = new ArrayList<>();
	}

	/**
	 * Initialized the evaluation instance. -> Loads all available queries -> Sets
	 * start values to all needed values
	 */
	public void init() {

		try {

			Class<?> searcherClass = Class.forName("searcher.Searcher" + corpusName.toUpperCase());
			this.search = (Searcher) searcherClass.newInstance();
			BufferedReader stopWords = new BufferedReader(new FileReader(stopwords.toFile()));

			search.init(DirectoryReader.open((FSDirectory.open(index))), stopWords);
			stopWords.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			XMLReader xmlReader = XMLReaderFactory.createXMLReader();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(new FileInputStream(input.toFile()), StandardCharsets.UTF_8));
			InputSource inputSource = new InputSource(reader);

			EvaluationContentHandler ich = new EvaluationContentHandler();
			xmlReader.setContentHandler(ich);
			xmlReader.parse(inputSource);

			this.inquiries = ich.getInquiries();

			reader.close();

		} catch (FileNotFoundException e) {
			System.err.println("Datei nicht gefunden. Datei wurde im folgenden Pfad gesucht: " + input.toString());
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}

		it = inquiries.iterator();
		request = Request.newBuilder();
		request.setResults(10);
		request.setPage(0);
	}

	/**
	 * Closed all open streams
	 */
	public void close() {
		try {
			this.search.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Saves all results in two files. One containing all queries and all relevant
	 * and not relevant docs (their id) One containing the precision recall value
	 */
	public void saveResults() {

		try {
			BufferedWriter bw = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(input.toFile()), StandardCharsets.UTF_8));

			bw.write(XMLTagsEvaluation.getIntro());
			bw.write(XMLTagsEvaluation.getDTD());
			bw.write(XMLTagsEvaluation.getBeginning());

			int i = 0;
			for (Inquiry inquiry : inquiries) {
				if (i % 10 == 0) {
					bw.write("\r\n");
				}
				i++;
				inquiry.ToXML(bw);
				bw.write("\r\n");
			}

			bw.write(XMLTagsEvaluation.getEnding());

			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			BufferedWriter bw = new BufferedWriter(
					new FileWriter(input.resolveSibling("precission.csv").toFile(), true));
			LocalDate date = java.time.LocalDate.now();

			bw.write(date.getDayOfMonth() + ". " + date.getMonth() + " " + date.getYear());

			for (Double precission : aap) {
				bw.write(";");
				bw.write(precission.toString());
			}

			bw.write("\r\n");

			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return the amount of available queries
	 */
	public int getQueryCount() {
		return inquiries.size();
	}

	/**
	 * Sets intern variables to access the next inquiry if available.
	 *
	 * @return true if a new inquiry is available
	 */
	public boolean nextInquery() {
		if (inquiry == null) {
			if (it.hasNext()) {
				inquiry = it.next();
				precission = new double[10];
				recall = new double[10];
				request.setQuery(inquiry.getQuery());
				try {
					laws = search.search(request.build());
					lawIt = laws.iterator();
				} catch (IOException e) {
					System.err.println("Critical Error. Search Engine doesn`t work.");
					e.printStackTrace();
				}

				countRelevantDoc = 0;
				position = -1;
				return true;
			}
			return false;
		}
		return true;
	}

	/**
	 * Proofs if an automatic decision for the relevance of this document could be
	 * make.
	 *
	 * @return true is the document is already classified as relevant or not
	 *         relevant for this inquiry
	 */
	public boolean relevanceDecidable() {
		if (inquiry.isRelevant(law.getDocnb())) {
			countRelevantDoc++;
			recall[position] = countRelevantDoc;
			precission[position] = (double) countRelevantDoc / (double) (position + 1);
			return true;
		} else if (inquiry.isNotRelevant(law.getDocnb())) {
			recall[position] = countRelevantDoc;
			precission[position] = (double) countRelevantDoc / (double) (position + 1);
			return true;
		}
		return false;
	}

	/**
	 * Sets the intern variable to the next law if available or compute the
	 * precision recall value and sets all intern variables to get the next inquiry
	 * if possible.
	 *
	 * @return true if a next law for the current inquiry is available.
	 */
	public boolean nextResult() {
		if (lawIt.hasNext()) {
			law = lawIt.next();
			position++;
			return true;
		} else {

			boolean hasRecall = false;
			for (int i = 0; i < laws.size(); i++) {
				hasRecall = hasRecall || recall[i] != 0;
				recall[i] /= countRelevantDoc;
			}

			double avaregePrecission = 0;
			if (hasRecall) {
				double maxPrecission = 0;
				int i = 0;

				for (double recallPoint : recallPoints) {
					maxPrecission = 0;
					int p;
					for (p = i; p < laws.size(); p++) {
						maxPrecission = (maxPrecission < precission[p]) ? precission[p] : maxPrecission;
					}

					while (recallPoint <= p && i < laws.size()) {
						avaregePrecission += maxPrecission;
						i++;
					}
				}
			}

			avaregePrecission /= 11;
			aap.add(avaregePrecission);
			inquiry = null;
			return false;
		}
	}

	/**
	 * If no automatic decision could be made the user has to make an input. Use
	 * this method if the user say the current document is relevant.
	 */
	public void UserSayIsRelevant() {
		inquiry.addRelevant(law.getDocnb());
		countRelevantDoc++;
		recall[position] = countRelevantDoc;
		precission[position] = (double) countRelevantDoc / (double) (position + 1);
	}

	/**
	 * If no automatic decision could be made the user has to make an input. Use
	 * this method if the user say the current document is not relevant.
	 */
	public void UserSayIsNotRelevant() {
		inquiry.addNotRelevant(law.getDocnb());
		recall[position] = countRelevantDoc;
		precission[position] = (double) countRelevantDoc / (double) (position + 1);
	}

	/**
	 *
	 * @return the current active law
	 */
	public Law getLaw() {
		return law;
	}

	/**
	 *
	 * @return the current active inquiry
	 */
	public Inquiry getInquiry() {
		return inquiry;
	}

}
