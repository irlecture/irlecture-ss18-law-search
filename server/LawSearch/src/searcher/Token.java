package searcher;

public class Token {
	private String token;
	private boolean isParagraph;

	public Token() {

	}

	public Token(String token, boolean isParagraph) {
		this.token = token;
		this.isParagraph = isParagraph;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean isParagraph() {
		return isParagraph;
	}

	public void setParagraph(boolean isParagraph) {
		this.isParagraph = isParagraph;
	}
}
