package searcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.Query;

import generated.LawSearchAPI.Law;
import generated.LawSearchAPI.Request;
import generated.LawSearchAPI.Response;

public interface Searcher {

	/**
	 * Initialized the Searcher. (Set IndexReader and stop words)
	 *
	 * @param reader
	 * @param stopWords
	 */
	public void init(IndexReader reader, BufferedReader stopWords);

	/**
	 * proof if the Searcher is initialized.
	 *
	 * @return true if initialized
	 */
	public boolean isInitialized();

	/**
	 *
	 * @param request
	 * @return
	 * @throws IOException
	 */
	List<Law> search(Request request, Response.Builder response) throws IOException;

	List<Law> search(Request request) throws IOException;

	void close() throws IOException;

	Query parseQuery(Request request);

	Document getDocument(int n) throws IOException;

	Law getDocument(String docNb) throws IOException;

}
