/**
 *
 */
package searcher;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.BoostQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.util.QueryBuilder;

import analyzer.AnalyzerBRD;
import generated.LawSearchAPI.Law;
import generated.LawSearchAPI.Request;
import generated.LawSearchAPI.Response;
import lawsearch.DocumentConstants;
import opennlp.tools.stemmer.Stemmer;
import opennlp.tools.stemmer.snowball.SnowballStemmer;
import opennlp.tools.stemmer.snowball.SnowballStemmer.ALGORITHM;

/**
 * @author Sven Landmann
 *
 */
public class SearcherBRD implements Searcher {
	IndexReader reader;
	IndexSearcher searcher;
	QueryBuilder queryBuilder;
	Stemmer stemmer;
	CharArraySet stopWordSet;
	boolean isInitialized = false;

	/*
	 * (non-Javadoc)
	 *
	 * @see searcher.Searcher#init( org.apache.lucene.index.IndexReader,
	 * java.io.InputStream)
	 */
	@Override
	public void init(IndexReader reader, BufferedReader pathToStopWords) {
		isInitialized = true;
		this.reader = reader;
		this.searcher = new IndexSearcher(reader);
		AnalyzerBRD analyzer = new AnalyzerBRD();
		try {
			analyzer.init(pathToStopWords);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.queryBuilder = new QueryBuilder(analyzer);
		stopWordSet = analyzer.getStopWordSet();
		this.stemmer = new SnowballStemmer(ALGORITHM.GERMAN);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see searcher.Searcher#search(generated.LawSearchAPI.Request)
	 */
	@Override
	public List<Law> search(Request request, Response.Builder response) throws IOException {
		Query query = parseQuery(request);
		TopDocs docs = null;
		if (request.getPage() > 1) {
			docs = this.searcher.searchAfter(new ScoreDoc(request.getDoc(), request.getScore()), query,
					request.getResults());
		} else {
			docs = this.searcher.search(query, request.getResults());
		}

		ArrayList<Law> resultFileNames = new ArrayList<>();
		Law.Builder law = Law.newBuilder();
		for (ScoreDoc document : docs.scoreDocs) {
			Document doc = this.searcher.doc(document.doc);
			law.setDocnb(doc.getField(DocumentConstants.DOCNR).stringValue());
			law.setHeadline(doc.getField(DocumentConstants.HEADLINE).stringValue());
			law.setLawbook(doc.getField(DocumentConstants.LAWBOOK).stringValue());
			law.setParagraph(doc.getField(DocumentConstants.PARAGRAPH).stringValue());
			law.setShortname(doc.getField(DocumentConstants.SHORTNAME).stringValue());
			law.setText(doc.getField(DocumentConstants.CONTENT).stringValue());
			resultFileNames.add(law.build());
			response.setDoc(document.doc);
			response.setScore(document.score);

			law.clear();
		}

		return resultFileNames;

	}

	@Override
	public List<Law> search(Request request) throws IOException {
		TopScoreDocCollector results;
		if (request.getPage() > 1) {
			results = TopScoreDocCollector.create(request.getResults(),
					new ScoreDoc(request.getDoc(), request.getScore()));
		} else {
			results = TopScoreDocCollector.create(request.getResults());
		}

		Query query = parseQuery(request);
		this.searcher.search(query, results);
		ArrayList<Law> resultFileNames = new ArrayList<>();

		TopDocs test = results.topDocs();
		Law.Builder law = Law.newBuilder();
		for (ScoreDoc docs : test.scoreDocs) {
			Document doc = this.searcher.doc(docs.doc);
			law.setDocnb(doc.getField(DocumentConstants.DOCNR).stringValue());
			law.setHeadline(doc.getField(DocumentConstants.HEADLINE).stringValue());
			law.setLawbook(doc.getField(DocumentConstants.LAWBOOK).stringValue());
			law.setParagraph(doc.getField(DocumentConstants.PARAGRAPH).stringValue());
			law.setShortname(doc.getField(DocumentConstants.SHORTNAME).stringValue());
			law.setText(doc.getField(DocumentConstants.CONTENT).stringValue());
			resultFileNames.add(law.build());
			law.clear();
		}

		return resultFileNames;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see searcher.Searcher#close()
	 */
	@Override
	public void close() throws IOException {
		reader.close();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see searcher.Searcher#parseQuery(generated.LawSearchAPI.Request)
	 */
	@Override
	public Query parseQuery(Request request) {
		return parseQuery4(request);
	}

	public Query parseQuery1(Request request) {
		BooleanQuery.Builder bQuery = new BooleanQuery.Builder();

		Query hQuery = this.queryBuilder.createBooleanQuery(DocumentConstants.HEADLINE, request.getQuery());
		Query cQuery = this.queryBuilder.createBooleanQuery(DocumentConstants.CONTENT, request.getQuery());

		bQuery.add(cQuery, BooleanClause.Occur.SHOULD);
		bQuery.add(hQuery, BooleanClause.Occur.SHOULD);

		return bQuery.build();
	}

	public Query parseQuery2(Request request) {

		BooleanQuery.Builder endQuery = new BooleanQuery.Builder();

		BooleanQuery.Builder specialistSearch = new BooleanQuery.Builder();

		BooleanQuery.Builder paragraphs = new BooleanQuery.Builder();
		BooleanQuery.Builder context = new BooleanQuery.Builder();
		BooleanQuery.Builder lawbook = new BooleanQuery.Builder();
		BooleanQuery.Builder shortname = new BooleanQuery.Builder();
		BooleanQuery.Builder headline = new BooleanQuery.Builder();

		List<String> importantTokens = new ArrayList<>();
		boolean hasParagraph = false;
		int i = 0;

		for (String tok : tokenizeQuery(request)) {
			if (!stopWordSet.contains(tok)) {
				if (tok.matches("^�") || (tok.matches("^Art"))) {
					paragraphs.add(new BoostQuery(new WildcardQuery(new Term(DocumentConstants.PARAGRAPH, tok)), 5.0f),
							Occur.SHOULD);
					hasParagraph = true;
				} else {
					String token = stemmer.stem(tok).toString().toLowerCase();
					if (i < 3) {
						importantTokens.add(token);
						i++;
					}

					context.add(new TermQuery(new Term(DocumentConstants.CONTENT, token)), Occur.SHOULD);

					headline.add(new TermQuery(new Term(DocumentConstants.HEADLINE, token)), Occur.SHOULD);
					shortname.add(new TermQuery(new Term(DocumentConstants.SHORTNAME, token)), Occur.SHOULD);
					lawbook.add(new TermQuery(new Term(DocumentConstants.LAWBOOK, token)), Occur.SHOULD);
				}
			}
		}

		switch (importantTokens.size()) {
		case 3:
			context.add(new PhraseQuery(100000, DocumentConstants.CONTENT, importantTokens.get(0),
					importantTokens.get(1), importantTokens.get(2)), Occur.SHOULD);
			context.add(
					new PhraseQuery(100000, DocumentConstants.CONTENT, importantTokens.get(1), importantTokens.get(2)),
					Occur.SHOULD);
			context.add(
					new PhraseQuery(100000, DocumentConstants.CONTENT, importantTokens.get(0), importantTokens.get(2)),
					Occur.SHOULD);
		case 2:
			context.add(
					new PhraseQuery(100000, DocumentConstants.CONTENT, importantTokens.get(0), importantTokens.get(1)),
					Occur.SHOULD);
			break;
		}

		endQuery.add(new BoostQuery(headline.build(), 1.5f), Occur.SHOULD);

		endQuery.add(context.build(), Occur.SHOULD);

		if (hasParagraph) {
			specialistSearch.add(lawbook.build(), Occur.SHOULD);
			specialistSearch.add(shortname.build(), Occur.SHOULD);
			specialistSearch.add(paragraphs.build(), Occur.MUST);
			endQuery.add(new BoostQuery(specialistSearch.build(), 10.0f), Occur.SHOULD);
			endQuery.add(paragraphs.build(), Occur.SHOULD);
		}

		endQuery.add(new BoostQuery(lawbook.build(), 5.0f), Occur.SHOULD);
		endQuery.add(new BoostQuery(shortname.build(), 5.0f), Occur.SHOULD);

		return endQuery.build();
	}

	private List<String> tokenizeQuery(Request request) {
		List<String> tokens = new ArrayList<>();

		for (String token : request.getQuery().split(" ")) {
			tokens.add(token);
		}

		return tokens;
	}

	public Query parseQuery3(Request request) {

		BooleanQuery.Builder endQuery = new BooleanQuery.Builder();

		BooleanQuery.Builder specialistSearch = new BooleanQuery.Builder();

		BooleanQuery.Builder paragraphs = new BooleanQuery.Builder();
		BooleanQuery.Builder context = new BooleanQuery.Builder();
		BooleanQuery.Builder lawbook = new BooleanQuery.Builder();
		BooleanQuery.Builder shortname = new BooleanQuery.Builder();
		BooleanQuery.Builder headline = new BooleanQuery.Builder();

		List<String> importantTokens = new ArrayList<>();
		boolean hasParagraph = false;
		int i = 0;

		for (Token tok : lawTokenizer(request.getQuery())) {
			if (!stopWordSet.contains(tok.getToken())) {
				if (tok.isParagraph()) {
					paragraphs.add(
							new BoostQuery(new WildcardQuery(new Term(DocumentConstants.PARAGRAPH, tok.getToken())),
									5.0f),
							Occur.SHOULD);
					hasParagraph = true;
				} else {
					String token = stemmer.stem(tok.getToken()).toString();
					if (i < 3) {
						importantTokens.add(token);
						i++;
					}

					context.add(new TermQuery(new Term(DocumentConstants.CONTENT, token)), Occur.SHOULD);
					headline.add(new TermQuery(new Term(DocumentConstants.HEADLINE, token)), Occur.SHOULD);
					shortname.add(new TermQuery(new Term(DocumentConstants.SHORTNAME, token)), Occur.SHOULD);
					lawbook.add(new TermQuery(new Term(DocumentConstants.LAWBOOK, tok.getToken())), Occur.SHOULD);
				}
			}
		}

		switch (importantTokens.size()) {
		case 3:
			context.add(new PhraseQuery(100000, DocumentConstants.CONTENT, importantTokens.get(0),
					importantTokens.get(1), importantTokens.get(2)), Occur.SHOULD);
			context.add(
					new PhraseQuery(100000, DocumentConstants.CONTENT, importantTokens.get(1), importantTokens.get(2)),
					Occur.SHOULD);
			context.add(
					new PhraseQuery(100000, DocumentConstants.CONTENT, importantTokens.get(0), importantTokens.get(2)),
					Occur.SHOULD);
		case 2:
			context.add(
					new PhraseQuery(100000, DocumentConstants.CONTENT, importantTokens.get(0), importantTokens.get(1)),
					Occur.SHOULD);
			break;
		}

		endQuery.add(new BoostQuery(headline.build(), 1.5f), Occur.SHOULD);

		endQuery.add(context.build(), Occur.SHOULD);

		if (hasParagraph) {
			specialistSearch.add(lawbook.build(), Occur.SHOULD);
			specialistSearch.add(shortname.build(), Occur.SHOULD);
			specialistSearch.add(paragraphs.build(), Occur.MUST);
			endQuery.add(new BoostQuery(specialistSearch.build(), 10.0f), Occur.SHOULD);
			endQuery.add(paragraphs.build(), Occur.SHOULD);
		}

		endQuery.add(new BoostQuery(lawbook.build(), 5.0f), Occur.SHOULD);
		endQuery.add(new BoostQuery(shortname.build(), 5.0f), Occur.SHOULD);

		return endQuery.build();
	}

	public Query parseQuery4(Request request) {

		BooleanQuery.Builder endQuery = new BooleanQuery.Builder();

		BooleanQuery.Builder specialistSearch = new BooleanQuery.Builder();

		BooleanQuery.Builder paragraphs = new BooleanQuery.Builder();
		BooleanQuery.Builder context = new BooleanQuery.Builder();
		BooleanQuery.Builder lawbook = new BooleanQuery.Builder();
		BooleanQuery.Builder shortname = new BooleanQuery.Builder();
		BooleanQuery.Builder headline = new BooleanQuery.Builder();

		List<String> importantTokens = new ArrayList<>();
		boolean hasParagraph = false;
		int i = 0;

		for (Token tok : lawTokenizer(request.getQuery())) {
			if (!stopWordSet.contains(tok.getToken())) {
				if (tok.isParagraph()) {
					paragraphs.add(
							new BoostQuery(new WildcardQuery(new Term(DocumentConstants.PARAGRAPH, tok.getToken())),
									5.0f),
							Occur.SHOULD);
					hasParagraph = true;
				} else {
					String token = stemmer.stem(tok.getToken()).toString();
					if (i < 3) {
						importantTokens.add(token);
						i++;
					}

					context.add(new TermQuery(new Term(DocumentConstants.CONTENT, token)), Occur.SHOULD);
					headline.add(new TermQuery(new Term(DocumentConstants.HEADLINE, token)), Occur.SHOULD);
					shortname.add(new TermQuery(new Term(DocumentConstants.SHORTNAME, token)), Occur.SHOULD);
					lawbook.add(new TermQuery(new Term(DocumentConstants.LAWBOOK, tok.getToken())), Occur.SHOULD);
				}
			}
		}

		switch (importantTokens.size()) {
		case 3:
			context.add(new PhraseQuery(100000, DocumentConstants.CONTENT, importantTokens.get(0),
					importantTokens.get(1), importantTokens.get(2)), Occur.SHOULD);
			context.add(
					new PhraseQuery(100000, DocumentConstants.CONTENT, importantTokens.get(1), importantTokens.get(2)),
					Occur.SHOULD);
			context.add(
					new PhraseQuery(100000, DocumentConstants.CONTENT, importantTokens.get(0), importantTokens.get(2)),
					Occur.SHOULD);
		case 2:
			context.add(
					new PhraseQuery(100000, DocumentConstants.CONTENT, importantTokens.get(0), importantTokens.get(1)),
					Occur.SHOULD);
			break;
		}

		endQuery.add(new BoostQuery(headline.build(), 0.5f), Occur.SHOULD);

		endQuery.add(context.build(), Occur.SHOULD);

		if (hasParagraph) {
			specialistSearch.add(lawbook.build(), Occur.SHOULD);
			specialistSearch.add(shortname.build(), Occur.SHOULD);
			specialistSearch.add(paragraphs.build(), Occur.MUST);
			endQuery.add(new BoostQuery(specialistSearch.build(), 10.0f), Occur.SHOULD);
			endQuery.add(paragraphs.build(), Occur.SHOULD);
		}

		endQuery.add(new BoostQuery(lawbook.build(), 5.0f), Occur.SHOULD);
		endQuery.add(new BoostQuery(shortname.build(), 5.0f), Occur.SHOULD);

		return endQuery.build();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see searcher.Searcher#getDocument(int)
	 */
	@Override
	public Document getDocument(int n) throws IOException {
		return this.searcher.doc(n);
	}

	@Override
	public Law getDocument(String docNb) throws IOException {
		TopScoreDocCollector collector = TopScoreDocCollector.create(1);
		this.searcher.search(new TermQuery(new Term(DocumentConstants.DOCNR, docNb)), collector);

		Law.Builder law = Law.newBuilder();
		Document doc = this.searcher.doc(collector.topDocs().scoreDocs[0].doc);

		law.setDocnb(doc.getField(DocumentConstants.DOCNR).stringValue());
		law.setHeadline(doc.getField(DocumentConstants.HEADLINE).stringValue());
		law.setLawbook(doc.getField(DocumentConstants.LAWBOOK).stringValue());
		law.setParagraph(doc.getField(DocumentConstants.PARAGRAPH).stringValue());
		law.setShortname(doc.getField(DocumentConstants.SHORTNAME).stringValue());
		law.setText(doc.getField(DocumentConstants.CONTENT).stringValue());
		return law.build();
	}

	@Override
	public boolean isInitialized() {
		return isInitialized;
	}

	public static List<Token> lawTokenizer(String query) {
		List<Token> tokenList = new ArrayList<>();
		boolean paragraph = false;
		String paragraphSymbol = "";
		String[] tokens = query.toLowerCase().split(" ");
		for (String token : tokens) {
			if (token.isEmpty())
				continue;

			if (token.matches("[0-9]+[a-z]*") && paragraph) {
				tokenList.add(new Token(paragraphSymbol + " " + token, true));
			} else if (token.matches("�") || token.matches("paragraph") || token.matches("paragraf")) {
				paragraphSymbol = "�";
				paragraph = true;
			} else if (token.matches("artikel") || token.matches("art")) {
				paragraphSymbol = "Art";
				paragraph = true;
			} else {
				paragraph = false;
				tokenList.add(new Token(token, false));
				splitCompoundWords(tokenList, token, "ministerium");
				splitCompoundWords(tokenList, token, "amt");
				splitCompoundWords(tokenList, token, "beh�rde");
				splitCompoundWords(tokenList, token, "wahl");
			}
		}
		return tokenList;
	}

	private static void splitCompoundWords(List<Token> tokens, String token, String wordpart) {
		if (token.matches(wordpart)) {
			tokens.add(new Token(wordpart, false));
			String[] tok = token.split(wordpart, 2);
			if (!tok[0].isEmpty())
				tokens.add(new Token(tok[0], false));
		}
	}

}
