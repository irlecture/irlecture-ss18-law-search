package analyzer;

import java.io.IOException;

import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import opennlp.tools.stemmer.Stemmer;
import opennlp.tools.stemmer.snowball.SnowballStemmer;
import opennlp.tools.stemmer.snowball.SnowballStemmer.ALGORITHM;

/**
 * Stems all tokens via a snowball stammer.
 * 
 * @author Sven Landmann
 *
 */
public class GermanStemmarFilter extends TokenFilter {
	CharTermAttribute term;
	private Stemmer stem;

	public GermanStemmarFilter(TokenStream input) {
		super(input);
		stem = new SnowballStemmer(ALGORITHM.GERMAN);

		this.clearAttributes();
		term = addAttribute(CharTermAttribute.class);
	}

	@Override
	public boolean incrementToken() throws IOException {
		if (input.incrementToken()) {
			String word = input.getAttribute(CharTermAttribute.class).toString();
			term.setEmpty();
			term.append(stem.stem(word));
			return true;
		}

		return false;
	}

}
