package analyzer;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

/**
 * A Token is defined as sequence between whitespace or punctuation symbols
 * (.,;:()[]"-)
 * 
 * @author Sven Landmann
 *
 */
public class XMLTokenizer extends Tokenizer {
	protected CharTermAttribute term;
	private boolean inTag = false;
	Set<Character> punctuation;

	public XMLTokenizer() {
		super();
		punctuation = new HashSet<>();
		punctuation.add('.');
		punctuation.add(',');
		punctuation.add(';');
		punctuation.add(':');
		punctuation.add('(');
		punctuation.add(')');
		punctuation.add('[');
		punctuation.add(']');
		punctuation.add('"');
		punctuation.add('-');

		this.clearAttributes();
		term = addAttribute(CharTermAttribute.class);

	}

	@Override
	// TODO inplement UTF-8 support
	public boolean incrementToken() throws IOException {
		this.clearAttributes();
		char ch;

		boolean isNotEmpty = false;
		term.setEmpty();

		while (true) {
			ch = (char) input.read();
			if (ch == (char) -1) {
				break;
			}

			if (ch == '<') {
				inTag = true;
				if (isNotEmpty) {
					return true;
				}
			} else if (ch == '>') {
				inTag = false;

			} else if (!inTag && (Character.isWhitespace(ch) || punctuation.contains(ch))) {
				if (isNotEmpty) {
					return true;
				}
			} else if (Character.isISOControl(ch)) {
				continue;
			} else if (!inTag) {

				isNotEmpty = true;
				term.append(ch);
			}
		}

		return isNotEmpty;
	}

	@Override
	public void reset() throws IOException {
		super.reset();
		inTag = false;

		this.clearAttributes();
	}
}
