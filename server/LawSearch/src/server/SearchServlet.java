package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.store.FSDirectory;

import com.google.protobuf.util.JsonFormat;

import generated.LawSearchAPI.Request;
import generated.LawSearchAPI.Response;
import generated.LawSearchAPI.errorCode;
import lawsearch.LawSearch;
import searcher.Searcher;

/**
 * Entry Point for the server - application. Defines the behavior of the server
 * for incoming request, log writing and server initializing.
 *
 * @author Sven Landmann
 *
 */
public class SearchServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private Logger clientLogger = Logger.getLogger("ClientLogger");
	private Logger statusLogger = Logger.getLogger("StatusLogger");
	private Logger requestLogger = Logger.getLogger("RequestLogger");

	private HashMap<String, LawSearch> laws;
	private String defaultKey;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SearchServlet() {
		super();
	}

	/**
	 * Reads the servlet's {@link ServletConfig} that contains the parameters
	 * defined in the servlet descriptor (web.xml).
	 *
	 * @see Servlet#init(ServletConfig)
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		clientLogger.setLevel(Level.INFO);
		statusLogger.setLevel(Level.WARNING);
		requestLogger.setLevel(Level.INFO);

		laws = new HashMap<>();
		defaultKey = config.getInitParameter(InitParameters.CORPUS_DEFAULT_KEY);

		List<Configuration> lawSearchConfiguration = loadConfigurations(config);

		for (Configuration lawSearchConfig : lawSearchConfiguration) {
			String serviceRegistryKey = lawSearchConfig.getProperty(InitParameters.CORPUS_REGISTRY_KEY);
			String corpusName = lawSearchConfig.getProperty(InitParameters.CORPUS_NAME);
			if (serviceRegistryKey == null) {
				statusLogger.log(Level.WARNING, "No service registry key is set in one Config!");
			} else if (corpusName == null) {
				statusLogger.log(Level.WARNING, "No corpus name is set in one config");
			} else if (laws.containsKey(serviceRegistryKey)) {
				statusLogger.log(Level.WARNING, "The key " + serviceRegistryKey + "can not used multiply times!");
			} else {
				LawSearch service;

				try {
					Class<?> searcherClass = Class.forName("searcher.Searcher" + corpusName.toUpperCase());
					Searcher searcher = (Searcher) searcherClass.newInstance();
					BufferedReader stopWords = new BufferedReader(
							new InputStreamReader((this.getClass().getClassLoader().getResourceAsStream(
									"../conf/" + lawSearchConfig.getProperty(Configuration.STOPWORDLIST)))));

					searcher.init(
							DirectoryReader.open(
									(FSDirectory.open(Paths.get(lawSearchConfig.getProperty(Configuration.PATH))))),
							stopWords);
					stopWords.close();
					service = new LawSearch(corpusName, searcher);
					laws.put(serviceRegistryKey, service);
					statusLogger.log(Level.INFO, String.format("Search engine registered. Key = %s name = %s",
							serviceRegistryKey, corpusName));
				} catch (ClassNotFoundException e1) {
					statusLogger.log(Level.WARNING, "Class searcher.Searcher%s does not exists. ",
							corpusName.toUpperCase());
					e1.printStackTrace();
				} catch (InstantiationException e) {
					statusLogger.log(Level.WARNING, "Class searcher.Searcher%s could not be instantiated",
							corpusName.toUpperCase());
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					statusLogger.log(Level.WARNING, e.getMessage());
					e.printStackTrace();
				} catch (IOException e) {
					statusLogger.log(Level.WARNING, e.getMessage());
					e.printStackTrace();
				}
			}
		}
		statusLogger.log(Level.INFO, "Groesse: " + laws.size());
		if (laws.isEmpty()) {
			statusLogger.log(Level.SEVERE, "No searchEngine could be initalize!");
			System.exit(-1);
		}

	}

	/**
	 * @throws IOException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

		Request rq = buildRequest(request);
		statusLogger.log(Level.INFO, rq.toString());
		if (rq.getQuery() != null) {
			statusLogger.log(Level.INFO, "Corpus available : " + laws.containsKey(rq.getCorpus()));
			Response rp = laws.get(rq.getCorpus()).search(rq);
			response.setContentType("text/json; charset=UTF-8");
			JsonFormat.printer().includingDefaultValueFields().appendTo(rp, response.getWriter());
			if (rq.getPage() == 1)
				logRequest(request);
		} else {
			Response.Builder rp = Response.newBuilder();
			rp.setCode(errorCode.noError);
			rp.setErrorText("");
			response.setContentType("text/json; charset=UTF-8");
			JsonFormat.printer().appendTo(rp, response.getWriter());
			logError(request, rp.build());
		}
	}

	/**
	 * @throws IOException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

		String docs = request.getParameter(UrlParameters.DOCS);
		String docnb = request.getParameter(UrlParameters.LOG);
		String time = request.getParameter(UrlParameters.TIME);
		statusLogger.log(Level.INFO, docnb);
		statusLogger.log(Level.INFO, docs);
		statusLogger.log(Level.INFO, time);

		if (docnb == null || time == null || docs == null) {
			response.setContentType("text/plain; charset=UTF-8");
			response.getWriter().write("failure");
			return;
		}

		clientLogger.log(Level.INFO, String.format("Query: %s Time used: %s clickedResults: %s", docnb, time, docs));

		response.setContentType("text/plain; charset=UTF-8");
		response.getWriter().write("success");
	}

	// TODO add advance Search Parameters
	private Request buildRequest(HttpServletRequest request) {
		Request.Builder rq = Request.newBuilder();

		String corpus = request.getParameter(UrlParameters.CORPUS);
		if (corpus == null || !laws.containsKey(corpus)) {
			rq.setCorpus(defaultKey);
		}

		String query = request.getParameter(UrlParameters.QUERY);
		if (query != null) {
			rq.setQuery(query);
		}

		String page = request.getParameter(UrlParameters.PAGE);
		int pag = 1;
		try {
			pag = Integer.parseInt(page);
		} catch (NumberFormatException nbE) {

		}
		rq.setPage(pag);

		String results = request.getParameter(UrlParameters.RESULTS);
		int res = 10;
		try {
			res = Integer.parseInt(results);
		} catch (NumberFormatException nbE) {

		}
		rq.setResults(res);

		String doc = request.getParameter(UrlParameters.DOCUMENT);
		int d = 0;
		try {
			d = Integer.parseInt(doc);
		} catch (NumberFormatException nbE) {
			rq.setPage(1);
		}
		rq.setDoc(d);

		String score = request.getParameter(UrlParameters.SCORE);
		float sc = 0;
		try {
			sc = Float.parseFloat(score);
		} catch (NumberFormatException nbE) {
			rq.setPage(1);
		} catch (NullPointerException npE) {
			rq.setPage(1);
		}
		rq.setScore(sc);

		return rq.build();
	}

	/**
	 * Logs an error, who occurs in the search process.
	 *
	 * @param request
	 * @param response
	 */
	private void logError(HttpServletRequest request, Response response) {
		if (response.getCode() != errorCode.noError && response.getCode() != errorCode.queryError) {
			statusLogger.info(String.format("IP: %s QUERY: %s ERRCODE: %d ERRMSG: %s", request.getRemoteAddr(),
					request.getParameter(UrlParameters.QUERY), response.getCode(), response.getErrorText()));
		}
	}

	private void logRequest(HttpServletRequest request) {
		final String ip = request.getRemoteAddr();
		final String query = request.getParameter(UrlParameters.QUERY);
		final String logMessage = String.format("IP: %s QUERY: %s", ip, query);

		requestLogger.info(logMessage);

	}

	/**
	 * Load each defined corpus (in web.xml). If no file could be loaded a fatal
	 * error are assumed and the program will be closed. If single files could not
	 * be loaded, no configuration will e created.
	 *
	 * @param config
	 *            the ServletConfig
	 * @return an List containing all available Configurations
	 */
	private List<Configuration> loadConfigurations(ServletConfig config) {
		List<Configuration> configurations = new ArrayList<>();

		statusLogger.log(Level.INFO, "Start loading property files");
		String[] confFiles = config.getInitParameter(InitParameters.CORPUS_CONFIGURATION_FILES).split(",");
		int countFileNotFound = 0;
		for (int i = 0; i < confFiles.length; i++) {
			statusLogger.log(Level.INFO, this.getClass().getClassLoader().getResource("").getPath());

			InputStream is = this.getClass().getClassLoader().getResourceAsStream("../conf/" + confFiles[i]);
			if (is == null) {

				statusLogger.log(Level.WARNING, "Could not find " + confFiles[i]);
				countFileNotFound++;
			} else {
				try {
					Configuration configuration = new Configuration();
					configuration.load(is);
					configurations.add(configuration);
					is.close();
				} catch (IOException ioE) {
					statusLogger.log(Level.SEVERE, "IOException occured. Critical Error.");
					System.exit(-1);
				}
			}
		}
		if (countFileNotFound == confFiles.length) {
			statusLogger.log(Level.SEVERE, "No configuration file could be loaded.");
			System.exit(-1);
		}

		statusLogger.log(Level.INFO,
				"Try to load " + confFiles.length + " files. " + countFileNotFound + " errors occur.");

		return configurations;
	}
}
