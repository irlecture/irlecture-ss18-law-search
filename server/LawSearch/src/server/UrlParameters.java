
package server;

/**
 * REST - Interface
 *
 * @author Sven Landmann
 *
 */
public interface UrlParameters {

	public static final String CORPUS = "corpus", // (string)
			RESULTS = "results", // (integer)
			PAGE = "page", // (integer)
			QUERY = "query", // (string)
			DOCUMENT = "doc", // (string/integer)
			SCORE = "score", // (score//float)
			LOG = "log";
	public static final String CLICKEDRESULTS = "clickedresults";
	public static final String TIME = "time";
	public static final String DOCS = "docs";
}
