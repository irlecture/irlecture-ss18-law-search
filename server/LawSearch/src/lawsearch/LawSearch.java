package lawsearch;

import java.io.IOException;
import java.util.List;

import generated.LawSearchAPI.Law;
import generated.LawSearchAPI.Request;
import generated.LawSearchAPI.Response;
import generated.LawSearchAPI.errorCode;
import searcher.Searcher;

/**
 *
 * @author Sven Landmann
 *
 */
public class LawSearch {
	private String name;
	private Searcher searcher;

	public LawSearch(String name, Searcher searcher) {
		this.name = name;
		this.searcher = searcher;
	}

	public String getName() {
		return this.name;
	}

	public Response search(Request request) {
		Response.Builder response = Response.newBuilder();
		try {
			List<Law> results = searcher.search(request, response);

			response.addAllResult(results);
			response.setPage(request.getPage());

		} catch (IOException e) {
			response.setCode(errorCode.serverError);
			response.setErrorText("Error");
		}

		return response.build();

	}

}
