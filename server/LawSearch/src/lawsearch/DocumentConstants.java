package lawsearch;

/**
 * Defines all Fields used for indexing and searching
 * 
 * @author Sven Landmann
 *
 */
public class DocumentConstants {
	public static final String DOCNR = "docnr";
	public static final String CONTENT = "content";
	public static final String SHORTNAME = "shortname";
	public static final String PARAGRAPH = "paragraph";
	public static final String LAWBOOK = "lawbook";
	public static final String HEADLINE = "headline";
}
