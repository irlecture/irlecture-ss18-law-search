# Installationsanleitung
## Inhaltsverzeichnis
1. Vorbedingungen
2. Erstellen eines neuen Indexes
3. lokale Suche (ohne Server)
4. Einrichtung des Servers
5. Zukunftsblick

### 1. Vorbedingungen (Betriebsystem Ubuntu 18.04)
- laden Sie zunächst das gesamte Repository herunter
- [Eclipse](http://www.eclipse.org/downloads/) downloaden
- die .zip entpacken, den Installer ausführen (Wählen Eclipse IDE für Java Developers)
- Konsole öffnen und folgende eingeben
    - sudo apt install openjdk-8-jdk
    - sudo apt install default-jdk
    - sudo apt install openjfx
    - sudo apt install tomcat8 (theoretisch müsste auch tomcat7 gehen (für ältere Ubuntu - Versionen))
- Project in eclipse öffnen und unter Windows -> Preferences -> Java -> Installed JREs  neue JRE hinzufügen (java-1.8.0-openjdk'cpu-unit')
- Konsole öffnen in /var/lib/ und folgendes eingeben
    - sudo chown -R <Benutzer> tomcat8 
    - sudo chgrp -R tomcat8 tomcat8
 
### 2. Erstellung eines neuen Indexes
1. Setzen Sie die Pfade in ConstantForUsage (package usage)
2. Vergewissern Sie sich, dass genügend Speicher zur Vrrfügug steht
3. Führen Sie DataToIndex.java aus.
4. Geben Sie ihren erezugten Index einen Namen (Verwenden Sie "brd", sonst funktioniert die Anwendung nicht), eine Sprache sowie einen Key (dieser muss eindeutig sein)

### 3. lokale Suche
1. Setzen Sie die Pfade und CorpusName (auf "brd") in ConstantForUsage (package usage)
2. Führen Sie die Console_Search.java aus 

Sie haben nun die Möglichkeit lokal zu suchen, wobei die Darstellung der Ergebnisse nicht gut ist(da diese für html ausgelegt sind). Ist eher zum ausprobieren und Testen gedacht.

### 4. Einrichten des Servers
1. führen Sie alle Schritte der Vorbedngung aus
2. Setzen Sie im Ordner <project>/war/WEB-INF/conf/<name>.property die Pfadangaben (falls der Index nicht neu erzeugt wurde) 
3. gehen sie auf <project>/build/build.xml, rechtsklicken darauf unf führen "Ant Build" aus
4. wenn der Bau erfolgreich war, ist im Ordner <project>/build/warbin/ eine .war erzeugt worden
5. Diese Datei kopieren und in <tomcat>/webapps hineinkopieren
6. Server starten

Sie haben nun, sofern kein Fehler aufgetreten ist, einen Server. Die Applikation können Sie dann über http://localhost:8080/lawsearch erreichen. Die Bedienung der Webseite ist anderen bekannten Suchmaschinen nachempfunden. 

### 5. Zukunftsblick
Die Applikation ist momentan nur für die Verwendung eines Corpus geeignet, allerdings wurden bereits erste anstrengungen unternommen, um weitere Corpi zuzulassen. Der Grund warum ein neu erzeugter Corpus "brd" heißen muss, ist, dass aus den Name die Searcher - Klasse abgeleitet wird. Wird ein anderer Name verwendet, wird entsprechend keine Klasse gefunden.
