
Erklärung der Ordner:

data - Gesetze als XML
rawData - Daten in ZIP-Archiven

Dokumentation - PDF zur Suchmaschine
presentation - Zwischenpräsentationen zur Suchmaschine

server - Software-Ordner (enthält README zur Installation)
webPage - HTML, CSS und JavaScript für Website

Evaluation - zur Bewertung gesammelte Daten (keine Auswertung)
wordFrequency - Stopwörter u.Ä.

